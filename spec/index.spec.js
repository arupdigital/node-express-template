process.env.NODE_ENV = 'test';
const app = require('../app');
const request = require('supertest')(app);
const {expect} = require('chai');

describe('', () => {
  it('', () => {
    return request
      .get('/api')
      .expect(200)
      .then(() => {
        expect('test').to.equal('test');
      });
  });
});