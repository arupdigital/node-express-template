const { PG_DB_NAME, PG_USERNAME, PG_PASSWORD, HOSTNAME, ARANGO_DB_NAME } = require('../config');
const Sequelize = require('sequelize');
const { Database } = require('arangojs');

const pgDB = new Sequelize(PG_DB_NAME, PG_USERNAME, PG_PASSWORD, {
  host: HOSTNAME,
  dialect: 'postgres',
  logging: false,
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }    
});

const arangoDB = new Database();

const connectToPgDB = () => {
  return pgDB
    .authenticate()
    .then(() => {
      console.log(`Successfully connected to Postgres DB (${PG_DB_NAME})`);
      return pgDB;
    })
    .catch(err => console.error('Unable to connect to Postgres DB:', err.message));
};

const connectToArangoDB = async () => {
  await arangoDB.useDatabase(ARANGO_DB_NAME);
  await arangoDB.login();
  console.log(`Successfully connected to ArangoDB (${ARANGO_DB_NAME})`);
  return arangoDB;
};

function getArangoDB () {
  return arangoDB;
}

function getGeoDB () {
  return pgDB;
}

function getArangoCol (name) {
  return getArangoDB().collection(name);
}

function getArangoGraph(name) {
  return getArangoDB().graph(name);
}

module.exports = {
  connectToPgDB,
  connectToArangoDB,
  getArangoDB,
  getArangoCol,
  getArangoGraph,
  getGeoDB
};
