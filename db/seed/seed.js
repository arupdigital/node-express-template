const { connectToArangoDB, connectToPgDB } = require('../db');
const seedArangoDB = require('./arango-seed');
const seedPgDB = require('./pg-seed');
const {arangoData, pgData} = require('../data')[process.env.NODE_ENV];

Promise.all([
  connectToArangoDB(),
  connectToPgDB()
])
  .then(([arangoDB, pgDB]) => {
    return Promise.all([seedArangoDB(arangoDB, arangoData), seedPgDB(pgDB, pgData)]);
  })
  .then(([arangoDB, pgDB]) => {
    console.log('Seeding complete');
    arangoDB.close();
    pgDB.close();
  })
  .catch(console.error);