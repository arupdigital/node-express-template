const {getArangoCol} = require('../db/db');

const arangoCollections = [
  {
    name: 'ArangoCollection',
    schema: require('./arango-model-example')
  }
];

const arangoColModels = arangoCollections.reduce((acc, col) => {
  acc[col.name] = getArangoCol(col.name);
  acc[col.name].schema = col.schema;
  return acc;
}, {});

module.exports = arangoColModels;