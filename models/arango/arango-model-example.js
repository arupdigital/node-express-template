const Joi = require('joi');

const schema = Joi.object().keys({
  a_string: Joi.string().min(3).max(30),
  an_optional_string: Joi.string().max(3).optional(),
  a_boolean: Joi.boolean().optional(),
  an_integer: Joi.number().integer().optional(),
  a_date: Joi.date().optional()
});

module.exports = schema;