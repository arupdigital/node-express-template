const db = require('../../db/db').getPgDB();
const Sequelize = require('sequelize');

module.exports = db.define('t_example', {
  e_id: {
    type: Sequelize.INTEGER,
    primaryKey: true
  },
  e_geometry: {
    type: Sequelize.GEOMETRY,
    allowNull: false
  }
});