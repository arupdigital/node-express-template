module.exports = {
  arango: require('./arango'),
  postgres: require('./postgres')
};