# Node + Express Template #
A repository containing boilerplate code for projects requiring an Express.js server.

## Prequisites ##
In order to begin using this project, you must first install both **Node.js** and **Git**.

## Getting Started ##
### Forking this Repository ###
1. Begin by clicking on **+** in the Bitbucket sidebar and selecting **Fork this repository** under **Get to work**.
2. Name and define other options for the fork using the **Fork** dialog. Documentation for these options can be found [here](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html).
3. Click **Fork Respository**. Bitbucket will now create your fork and open the **Overview** page for the new project.

### Cloning the Fork ###
1. To clone the new project, click **Clone** in the top-right corner of the overview page.
2. Copy the command beginning `git clone` and run it from the terminal in the desired directory. Alternatively, select **Clone in Sourcetree**.

## Installing Dependencies
Run `npm i` in the project root directory to install all necessary dependencies.

## Starting the Server ##
To start the server for development purposes, run `npm run dev` in the project root directory.

## Browsing API Documentation ##
The Express app is preconfigured to listen on port **9090**. To browse the template API documentation, navigate to `locahost:9090/api`.

## Running Tests ##
This project is preconfigured to run tests defined in `/spec`. The **Mocha** framework is used for testing, along with **Chai** and **Supertest** assertion libraries.