const handle403 = (err, req, res, next) => {
  if (err.isBoom && err.output.statusCode === 403) {
    const { message, statusCode } = err.output.payload;
    res.status(403).send({ message, statusCode });
  }
  else if (err.isArangoError && err.statusCode === 403) {
    const { code: statusCode, errorMessage: message } = err.response.body;
    res.status(403).send({ message, statusCode });
  }
  else next(err);
};

module.exports = handle403;