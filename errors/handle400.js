const handle400 = (err, req, res, next) => {
  if (err.isBoom && err.output.statusCode === 400) {
    const { message, statusCode } = err.output.payload;
    res.status(400).send({ message, statusCode });
  }
  else if (err.isArangoError && err.statusCode === 400) {
    const { code: statusCode, errorMessage: message } = err.response.body;
    res.status(400).send({ message, statusCode });
  }
  else if(
    err.name === 'SequelizeDatabaseError' ||
    err.name === 'SequelizeValidationError' ||
    err.name === 'SequelizeUniqueConstraintError'
  ) {
    if(err.errors) {
      const message = err.errors.map(error => error.message).join(', ');
      res.status(400).send({statusCode: 400, message});
    }
    else res.status(400).send({message: err.message, statusCode: 400});
  }
  else next(err);
};

module.exports = handle400;