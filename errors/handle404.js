const handle404 = (err, req, res, next) => {
  if (err.isBoom && err.output.statusCode === 404) {
    const { message, statusCode } = err.output.payload;
    res.status(404).send({ message, statusCode });
  }
  else if (err.isArangoError && err.statusCode === 404) {
    const { code: statusCode, errorMessage: message } = err.response.body;
    res.status(404).send({ message, statusCode });
  }
  else next(err);
};

module.exports = handle404;