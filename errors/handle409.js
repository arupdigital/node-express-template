const handle409 = (err, req, res, next) => {
  if(err.code === 409) {
    res.status(409).send({statusCode: err.code, message: err.message});
  }
  else next(err);
};

module.exports = handle409;