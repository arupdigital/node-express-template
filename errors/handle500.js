const { internal } = require('boom');

const handle500 = (err, req, res, next) => {
  const defaultError = internal(err.message || 'Internal server error');
  res.status(500).send({error: defaultError});
  next;
};

module.exports = handle500;