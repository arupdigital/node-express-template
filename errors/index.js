module.exports = {
  handle400: require('./handle400'),
  handle403: require('./handle400'),
  handle404: require('./handle404'),
  handle409: require('./handle409'),
  handle500: require('./handle500'),
};