const { notFound, badRequest } = require('boom');
const ArangoModel = require('../models').arango.ArangoCollection;

function getAllArango (req, res, next) {
  const page = Number(req.query.page);
  const limit = Number(req.query.limit);
  if(page && limit) {
    if(
      !Number.isInteger(page) ||
      !Number.isInteger(limit) ||
      page < 1 ||
      limit < 1
    ) {
      throw badRequest('Invalid page request');
    }
    const skip = (page - 1) * limit;  
    ArangoModel.count()
      .then(({count}) => {
        const totalPages = Math.ceil(count / limit);
        if(page > totalPages) throw notFound('Total pages exceeded');
        else return Promise.all([ArangoModel.all({skip, limit}), totalPages]);
      })
      .then(([docs, totalPages]) => res.status(200).send({
        data: docs._result,
        page,
        totalPages
      }))
      .catch(next);
  } else {
    ArangoModel.all()
      .then((docs) => {
        res.status(200).send({data: docs._result});
      })
      .catch(next);
  }
}

function postArango (req, res, next) {
  const data = req.body;
  validate(data, ArangoModel.schema, 'post');
  ArangoModel.save(data)
    .then(doc => res.status(200).send(doc))
    .catch(next);
}

function validate (data, schema, method) {
  const options = {
    abortEarly: false
  };
  if(method !== 'patch') options.presence = 'required';
  const valRes = schema ?
    schema.validate(data, options) : null;
  if(valRes && valRes.error) {
    const errors = valRes.error.details.map(err => err.message);
    throw badRequest(errors);
  }
}

module.exports = {
  getAllArango,
  postArango
};