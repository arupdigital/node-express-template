const {notFound, badRequest} = require('boom');
const {PgModel} = require('../models').postgres;

function getAllPg (req, res, next) {
  const page = Number(req.query.page);
  const limit = Number(req.query.limit);
  if(page && limit) {
    if(
      !Number.isInteger(page) ||
      !Number.isInteger(limit) ||
      page < 1 ||
      limit < 1
    ) {
      throw badRequest('Invalid page request');
    }
    PgModel.findAndCountAll({
      limit,
      offset: limit * (page - 1)
    })
      .then(result => {
        const interests = result.rows;
        const totalPages = Math.ceil(result.count / limit);
        if(page > totalPages) throw notFound('Total pages exceeded');
        res.status(200).send({interests, page, totalPages});
      })
      .catch(next);
  } else {
    PgModel.findAll()
      .then(interests => res.status(200).send({interests}))
      .catch(next);
  }
}

module.exports = {
  getAllPg
};