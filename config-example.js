process.env.NODE_ENV = process.env.NODE_ENV || 'dev';

const config = {
  dev: {
    ARANGO_DB_URL: 'https://localhost:8529/',
    ARANGO_DB_NAME: 'some_arango_db',
    HOSTNAME: 'localhost',
    PG_DB_NAME: 'some_pg_db',
    PG_USERNAME: 'username',
    PG_PASSWORD: 'password'
  },
  test: {
    ARANGO_DB_URL: 'https://localhost:8529/',
    ARANGO_DB_NAME: 'some_arango_db_test',
    HOSTNAME: 'localhost',
    PG_DB_NAME: 'some_pg_db_test',
    PG_USERNAME: 'username',
    PG_PASSWORD: 'password'
  }
};

module.exports = config[process.env.NODE_ENV];