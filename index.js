const app = require('./app');
const { PORT = 9090 } = process.env;
const { connectToPgDB, connectToArangoDB } = require('./db/db');

Promise.all([
  connectToArangoDB(),
  connectToPgDB()
])
  .then(() => {
    app.listen(PORT, (err) => {
      if (err) throw err;
      console.log(`Listening on port ${PORT}`);
    });
  });