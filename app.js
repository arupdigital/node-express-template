const app = require('express')();
const bodyParser = require('body-parser');
const cors = require('cors');
const apiRouter = require('./routes/api');
const { notFound } = require('boom');
const { handle400, handle403, handle404, handle409, handle500 } = require('./errors');

app.use(cors());
app.use(bodyParser.json());

app.use('/api', apiRouter);

app.use('/*', (req, res, next) => next(notFound('Path not found')));

app.use(handle400);
app.use(handle403);
app.use(handle404);
app.use(handle409);
app.use(handle500);

module.exports = app;