const router = require('express').Router();
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('../swagger.json');

router.get('/', (req, res) => res.status(200).send({test: 'test'}));

router.use('/explore', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

module.exports = router;